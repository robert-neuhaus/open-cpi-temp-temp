OCPIDRIVER(1)
============


NAME
----
ocpidriver - command-line utility for managing the OpenCPI
loadable OpenCPI Linux kernel driver module


SYNOPSIS
--------
*`ocpidriver`* '<verb>'

DESCRIPTION
-----------
The *`ocpidriver(1)`* tool controls the OpenCPI Linux kernel device driver
module on an 'OpenCPI system': a collection of processors that can be used together
as resources for running OpenCPI applications. Systems with platforms
connected to a system bus, such as PCI Express or the Xilinx Zynq AXI
interfaces between the CPU (PS) and the FPGA (PL) parts of the SoC,
require the device driver when the bus or fabric is used for communication
between the host CPU platform and another platform.  Systems that do not
require this type of communication do not need the driver. See the section
on the OpenCPI Linux kernel device driver in the 'OpenCPI User Guide' for details.
The *`ocpidriver`* tool is used to load and control the driver
when it is required for platform communication on a system.

Some systems and kernels support the "Secure Boot" feature.
If the OpenCPI Linux kernel device driver is needed for these
systems, be sure to disable the "Secure Boot" feature if it has been enabled.

For convenience, options can occur anywhere in the command.
The general usage concept for *`ocpidriver`* is:
perform the '<verb>' operation on the
'<noun>' whose name is '<name>'.
When invoked without any verbs and options, *`ocpidriver`* displays help information.

VERBS
-----
The verbs supported by *`ocpidriver`* are:

*`load`*::
    Load an OpenCPI Linux kernel device driver to a system.
    
*`reload`*::
    Remove an OpenCPI Linux kernel device driver from a system and then load it.

*`status`*::
    Get the status of an OpenCPI Linux kernel device driver on a system.

*`unload`*::
    Remove an OpenCPI Linux kernel device driver from a system.

Performing these operations requires *`root`*/*`sudo`* privileges.

NOUNS
-----
None.

OPTIONS
-------
The general-purpose options common to all OpenCPI tools
can be used with the *`ocpidriver`* command.
See link:opencpi.1.html[opencpi(1)]).

EXAMPLES
--------
. Using the *`root`* account, load the OpenCPI Linux kernel device driver
on a system that has the Intel/Altera Stratix IV (*`alst4`*) and Xilinx (*`ml605`*) PCI Express
platforms installed and which has reserved an additional 128KB of DMA memory for the driver:
+
---------------
ocpidriver load
---------------
+
On success, the tool generates the following message:
+
----------------------------------------------------------------
Found generic reserved DMA memory on the linux boot command line
and assuming it is for OpenCPI: [memmap=128M$0x1000000]
Driver loaded successfully.
----------------------------------------------------------------
+
Now perform the command:
+
--------------
ocpirun --list
--------------
+
The output from the command is:
+
-------------------------------------------------------
Available containers:
   #  Model Platform   OS    OS-Version   Arch   Name
   0  hdl   ml605                                PCI:0000:08:00.0
   1  hdl   alst4                                PCI:0000:03:00.0
   2  rcc   centos7    linux c7           x86_64 rcc0
-------------------------------------------------------
+
. Unload the OpenCPI Linux kernel device driver:
+
------------------
ocpidriver unload
------------------
+
On success, the tool returns the message:
+
--------------------------------------------
The driver module was successfully unloaded.
--------------------------------------------

BUGS
----
See https://www.opencpi.org/report-defects


RESOURCES
---------
See the main web site: https://www.opencpi.org

See the 'OpenCPI Application Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Application_Development_Guide.pdf

See the 'OpenCPI Installation Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf


See the 'OpenCPI User Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_HDL_User_Guide.pdf


SEE ALSO
--------
link:ocpidev.1.html[ocpidev(1)]
link:ocpidev-application.1.html[ocpidev-application(1)]
link:ocpidev-card.1.html[ocpidev-card(1)]
link:ocpidev-platform.1.html[ocpidev-platform(1)]
link:ocpidev-slot.1.html[ocpidev-slot(1)]
link:ocpirun.1.html[ocpirun(1)]

COPYING
-------
Copyright \(C) 2023 OpenCPI www.opencpi.org. OpenCPI is free software:
you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.
