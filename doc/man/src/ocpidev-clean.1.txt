OCPIDEV-CLEAN(1)
================


NAME
----
ocpidev-clean - cleans OpenCPI assets


SYNOPSIS
--------
*`ocpidev`* ['<options>'] *`clean`* ['<noun>' ['<name>' ]]


DESCRIPTION
-----------
The *`clean`* verb cleans the OpenCPI assets specified by the noun used
in the command. If no nouns are provided, *`ocpidev`* cleans the current
directory. When the plural version of the nouns are used, no name is
specified and all assets of that type are cleaned.

The *`clean`* operation cleans the results of a *`build`* operation,
including any generated/built document files. The operation also cleans
the execution results of a *`run`* operation.

Asset types to be specified in the '<noun>' argument are:

*`application`*(*`s`*)::
    Clean all applications or a specific ACI application.

*`hdl`*::
    A prefix to indicate an HDL asset in the *hdl* subdirectory of a project.
    Possible HDL assets are:

        *`assembl`*(*`y`*|*`ies`*);;
	Clean all or a specified HDL assembly.
	
	*`device`*;;
	Clean an HDL device worker in a
	specified library.
	
	*`platform`*(*`s`*);;
	Clean all or a specified HDL platform.
	
	*`primitive`*(*`s`*);;
	Clean all or a specified HDL primitive of a specified type.
	For a specified HDL primitive, possible HDL primitive types are:
	
	    *`library`*:::
	    Clean a primitive that results in a library of modules.
	    
	    *`core`*:::
	    Clean a primitive that results in a synthesized
	    core/netlist.

*`librar`*(*`y`*|*`ies`*)::
    Clean all or the specified component library.

*`project`*::
    Clean all assets in a project.

*`test`*(*`s`*)::
    Clean all or a specified component unit test but do not run.

*`worker`*(*`s`*)::
    Clean all or the specified worker.  

Unless suppressed by options, the *`clean`* verb removes
both the built assets and their built OpenCPI asset documents from the project.
See the 'OpenCPI Documentation Writer Guide' for details on OpenCPI asset documents.

OPTIONS
-------
In addition to the options common to all OpenCPI tools (see link:opencpi.1.html[opencpi(1)]),
the following options can be specified for the *`clean`* verb.

Options When Cleaning Any Asset
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`--doc-only`*::
   Only clean the asset's documentation and not the asset itself.

*`--no-doc`*::
   Only clean the asset and not the asset's documentation.

Options When Cleaning Tests
~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`--simulation`*::
    Only clean the outputs from simulation, not any results of building or execution.  Simulation outputs can be
    very large.  This removes them without removing other potentially useful outputs.
    
*`--execute`*::
    Only clean the outputs from test execution, not building.  This 'does' include simulation outputs.

EXAMPLES
--------
. Clean the asset represented by the current directory and all the assets underneath it.
+
----------
ocpidev clean
----------
+
. Clean only the asset documents in the *`assets`* project. Omit the name *`assets`* if
inside the *`assets`* project.
+
---------------------------------------
ocpidev clean project assets --doc-only
---------------------------------------
+			 
. Clean the *`dsp_comps`* library
located at *`components/dsp_comps`* in the project
implied by the current directory.
+
-------------------------------
ocpidev clean library dsp_comps
-------------------------------

BUGS
----
See https://www.opencpi.org/report-defects

RESOURCES
---------
See the main web site: https://www.opencpi.org

See the 'OpenCPI Application Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Application_Development_Guide.pdf

See the 'OpenCPI Component Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Component_Development_Guide.pdf

See the 'OpenCPI HDL Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_HDL_Development_Guide.pdf

See the 'OpenCPI Platform Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Platform_Development_Guide.pdf

See the 'OpenCPI RCC Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_RCC_Development_Guide.pdf

See the 'OpenCPI Documentation Writer Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Documentation_Writer_Guide.pdf

SEE ALSO
--------
link:ocpidev.1.html[ocpidev(1)]
link:ocpidev-build.1.html[ocpidev-build(1)]
link:ocpidev-create.1.html[ocpidev-create(1)]
link:ocpidev-delete.1.html[ocpidev-delete(1)]
link:ocpidev-refresh.1.html[ocpidev-refresh(1)]
link:ocpidev-register.1.html[ocpidev-register(1)]
link:ocpidev-run.1.html[ocpidev-run(1)]
link:ocpidev-set.1.html[ocpidev-set(1)]
link:ocpidev-show.1.html[ocpidev-show(1)]
link:ocpidev-unregister.1.html[ocpidev-unregister(1)]
link:ocpidev-unset.1.html[ocpidev-unset(1)]
link:ocpidev-utilization.1.html[ocpidev-utilization(1)]


COPYING
-------
Copyright \(C) 2023 OpenCPI www.opencpi.org. OpenCPI is free software:
you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.



